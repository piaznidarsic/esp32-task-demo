#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "freertos/queue.h"

#define sent_message_lenght 5
#define received_message_lenght 5

// task declaration
void task_1 (void *in_parameter);
void task_2 (void *in_parameter);
void task_3 (void *in_parameter);
void task_4 (void *in_parameter);

// task handles declaration
TaskHandle_t task_1_handle = NULL;
TaskHandle_t task_2_handle = NULL;
TaskHandle_t task_3_handle = NULL;
TaskHandle_t task_4_handle = NULL;

//task priority variables decalration and inicialization (the higher the number, the higher the priority)
UBaseType_t task_1_priority = 2;
UBaseType_t task_2_priority = 1;
UBaseType_t task_3_priority = 0;
UBaseType_t task_4_priority = 2;

//task delays (1 = 1 tick, 100 ticks = 1s)
TickType_t task_1_tick_delay = 300;
TickType_t task_2_tick_delay = 100;
TickType_t task_3_tick_delay = 100;

//queues handle declaration
QueueHandle_t queue_1_handle;

uint32_t sent_message[5];
uint32_t received_message[5];

void app_main(void)
{
    printf("App main start.\n");

    // create tasks 
    xTaskCreate(task_1, "task_1", 2048, NULL, task_1_priority, &task_1_handle);
    xTaskCreate(task_2, "task_2", 2048, NULL, tasfk_2_priority, &task_2_handle);
    xTaskCreate(task_3, "task_3", 2048, NULL, task_3_priority, &task_3_handle);
    xTaskCreate(task_4, "task_4", 2048, NULL, task_4_priority, &task_4_handle);

    queue_1_handle = xQueueCreate(5,sizeof(uint32_t)); // queue creation (size = 5x uint32_t)

}

void task_1 (void *in_parameter)
{
    uint32_t value_to_send = 1034;

    while (true)
    {
        printf("Task_1 execution.\n");
        printf("Sent message: ");

        for(int i = 0; i < sent_message_lenght; i++)
        {
            if(queue_1_handle != NULL)
            {
                sent_message[i] = i;

                if(xQueueSend(queue_1_handle, &sent_message[i], portMAX_DELAY))
                {
                    printf("%d ", sent_message[i]);
                }
            }
        }

        printf("\n");

        xTaskNotify(task_4_handle, value_to_send, eNoAction);

        vTaskDelay(task_1_tick_delay);
    }
}

void task_2 (void *in_parameter)
{
    while (true)
    {
        printf("Task_2 execution.\n");

        vTaskDelay(task_2_tick_delay);
    }
}

void task_3 (void *in_parameter)
{
    while (true)
    {
        printf("Task_3 execution.\n");

        vTaskDelay(task_3_tick_delay);
    }
}

void task_4 (void *in_parameter)
{
    uint32_t in_notification_value = 0;
    BaseType_t notify_result = 0;

    while (true)
    {
        notify_result = xTaskNotifyWait(0x00,0xffffffff, &in_notification_value, portMAX_DELAY);

        if(notify_result == true)
        {
            printf("Task_4 execution.\n");

            printf("Received message: ");

            for(int i = 0; i < received_message_lenght; i++)
            {
                if(xQueueReceive(queue_1_handle, &received_message[i], portMAX_DELAY))
                {
                    printf("%d ", received_message[i]);
                }           
            }   
            printf("\n");
        }
    }
}
