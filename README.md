# Esp32 tasks demo

Esp32 tasks demo and message queue demonstration

## Prerequisites

- ESP-IDF development framework installed. Follow the [ESP-IDF installation guide](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html) to set up the development environment.
- FreeRTOS library, included in the ESP-IDF framework.

## Installations

1. Clone or download the project repository.

```console
git clone https://gitlab.com/magisterij1/semester_2/vseprisotno-racunalnistvo/esp32-task-demo.git
```

2. Change directory to the project folder.

```console
cd esp32_tasks_demo
```

3. Configure the project.

```console
idf.py menuconfig
```

4. Build the project.

```console
idf.py build
```

or

`ctrl + E, B`

5. Flash the firmware to your ESP32 device.

```console
idf.py -p <serial-port> flash
```

Replace <serial-port> with the appropriate serial port of your ESP32 device.

or

`ctrl + E, F`

## Usage

1. Connect your ESP32 device to your development machine.

2. Open a serial terminal to monitor the device output.

3. Power on the ESP32 device.

4. The program consists of four tasks:

- task_1: Sends messages to a queue and notifies task_4.
- task_2 and task_3: Idle tasks with predefined delays.
- task_4: Receives messages from the queue and prints them.

5. After flashing the firmware, the program will start executing on the ESP32 device.

6. The serial terminal will display the execution log of each task, including sent and received messages.

7. The program demonstrates inter-task communication using a queue and task notifications.

8. Customize the program logic and behavior as per your requirements.

## Acknowledgments

- [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/) - ESP32 development framework
- [FreeRTOS](https://www.freertos.org/) - Real-time operating system

## Troubleshooting

If you encounter any issues or errors, try the following steps:

- Ensure that all prerequisites are installed and configured correctly.
- Double-check the ESP32 device connections and power supply.
- Review the project's documentation and verify your code against the provided example.
- Search for similar issues or errors in the ESP-IDF community forums or GitHub repository.

If the issue persists, please open an issue with a detailed description of the problem, including any error messages or logs.
